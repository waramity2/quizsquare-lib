import React, { createElement } from "react";
import { View, Image, Text, Button } from "react-native";
import _ from "lodash";

import AutoHeightWebView from "react-native-autoheight-webview";

import { default as Sound } from "react-native-sound";
Sound.setCategory("Playback", true);

let md = require("markdown-it")(),
  mk = require("markdown-it-katex");
md.use(mk);

class QuizSquare extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      url: "",
      status: false,
			statusTitle: 'Play'
    };
    this.playSound = this.playSound.bind(this);
  }

  readLines(content) {
    let lines = content.split("\n"),
      result = {},
      wrap = [],
      tmpMarkdown = "";

    for (let i = 0; i < lines.length; i++) {
      if (this.isImage(lines[i])) {
				tmpMarkdown = this.handleMarkdownKatex(tmpMarkdown, wrap);
        wrap.push(this.handleImageSyntax(lines[i]));
      } else if (this.isSound(lines[i])) {
				tmpMarkdown = this.handleMarkdownKatex(tmpMarkdown, wrap);
        wrap.push(this.handleSoundFile(lines[i]));
      } else if(this.isAudioControl(lines[i])) {
        lines[i] = lines[i].replace("<audio controls>", "")
      } else {
        tmpMarkdown += lines[i] + "\n";
      }
      tmpMarkdown = this.handleMarkdownKatex(tmpMarkdown, wrap);
    }
    return wrap;
  }

  isImage(line) {
    let imgExpr = /!\[\]/;
    return imgExpr.test(line);
  }

  isSound(line) {
    // let soundExpr = /<source/; // image markdown regexr detection
    let soundExpr = /<source\s+(.*)\.mp3/;
    return soundExpr.test(line);
  }

  isAudioControl(line) {
    let audioCtrlExpr = /audio/;
    return audioCtrlExpr.test(line);
  }

  isHttp(uri) {
    let httpExpr = /http:/;
    return httpExpr.test(uri)
  }

  handleImageSyntax(src) {
		let imageUri = src.match(/http(.*)[^\)]/);
    if(this.isHttp(imageUri[0]))
      imageUri[0] = imageUri[0].replace("http", "https")
    return <Image style={{ width: 200, height: 200 }} source={{ uri: imageUri[0] }} />;
  }

  handleSoundFile(uri) {
		let soundUri = uri.match(/http(.*)\.mp3/);
    if(this.isHttp(soundUri[0]))
      soundUri[0] = soundUri[0].replace("http", "https")
    return <Button title={this.state.statusTitle} onPress={() => this.playSound(soundUri[0])} />;
  }

  playSound(uri) {
    this.setState({
      status: this.state.status ? false : true
    });
    const sound = new Sound(uri, null, error => {
      if (this.state.status === false) {
				this.setState({
					statusTitle: 'Play'
				})
				sound.stop();
			}
      else if (this.state.status === true) {
				this.setState({
					statusTitle: 'Stop'
				})
				sound.play();
			}
    });
  }

  handleMarkdownKatex(tmpMarkdown, wrap) {
		if(tmpMarkdown != "") {
    tmpMarkdown = md.render(tmpMarkdown);
    tmpMarkdown = `
			<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.5.1/katex.min.css">
			<link rel="stylesheet" href="https://cdn.jsdelivr.net/github-markdown-css/2.2.1/github-markdown.css"/>
			${tmpMarkdown}
		`;
		wrap.push(<AutoHeightWebView source={{ html: tmpMarkdown }} />);
		return ""
	}
		return tmpMarkdown;
  }

  render() {
    var prebuild = this.readLines(this.props.html);
    return <View>{prebuild}</View>;
  }
}

export default QuizSquare;
