import React, { Component } from "react";
import { View, Text } from "react-native";
import QuizTitle from "./components/QuizTitle";
import QuizContent from "./components/QuizContent";
import QuizSquare from "./lib/QuizSquare";

const styles = {
  foo: {
    fontSize: 50,
    color: "#007aff"
  }
};

export default class extends Component {

  constructor(props) {
    super(props);
    this.state = {
      question: this.props.data.questions[0],
    }
  }

  render() {
    return (
      <View>
        <Text>{this.props.data.title}</Text>
        <QuizTitle title="Test QuizTitle jaa" />
        <QuizContent content={this.state.question.title} />
        <Text>KUY SUS</Text>
      </View>
    );
  }
}
