import React, { Component } from 'react';
import {
  View,
  Text
} from 'react-native';

export default class QuizTitle extends Component<Props> {
  render() {
    return(
      <View>
        <Text>{this.props.title}</Text>
      </View>
    );
  }
}
