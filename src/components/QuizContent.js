import React, { Component } from "react";
import { View } from "react-native";
import QuizSquare from "../lib/QuizSquare";

export default class QuizContent extends Component<Props> {
  render() {
    return (
      <View>
        <QuizSquare html={this.props.content} />
      </View>
    );
  }
}
